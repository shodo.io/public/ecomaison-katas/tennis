plugins {
    kotlin("jvm") version "1.8.21"
    application
}

group = "fr.ecomobilier.kata"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
    testImplementation ("org.assertj:assertj-core:3+")
    testImplementation ("org.junit.jupiter:junit-jupiter-api:5+")
    testImplementation ("org.junit.jupiter:junit-jupiter-params:5+")
}


tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(8)
}

application {
    mainClass.set("MainKt")
}